#include "bmp.h"

int8_t BMP_Load(void **ptr, char *filename)
{
	uint32_t file_size;
	FILE * bmp_file;
	
	printf("Opening `%s` \r\n", filename);
	
	bmp_file = fopen(filename, "rb");
	fseek(bmp_file, 0, SEEK_END);
	file_size = ftell(bmp_file);
	
	printf("Attempting to malloc %ld bytes\n", file_size);
	*ptr = malloc(file_size);
	if(ptr == NULL) return E_OUTOFMEMORY;
	
	fseek(bmp_file, 0, SEEK_SET);
	uint32_t bytes_read = fread(*ptr, 1, file_size, bmp_file);
	printf("Read %ld bytes into buffer %p\n", bytes_read, *ptr);
	
	// Now fix the endianness of the header fields...
	BMPImage *image = *ptr;
	image->header.offset = EndianSwapU32(image->header.offset);
	image->header.type = EndianSwapU16(image->header.type);
	image->header.size = EndianSwapU32(image->header.size);
	image->header.dib_header_size = EndianSwapU32(image->header.dib_header_size);
	image->header.width_px = EndianSwapU32(image->header.width_px);
	image->header.height_px = EndianSwapU32(image->header.height_px);
	image->header.num_planes = EndianSwapU16(image->header.num_planes);
	image->header.bits_per_pixel = EndianSwapU16(image->header.bits_per_pixel);
	image->header.compression = EndianSwapU32(image->header.compression);
	image->header.image_size_bytes = EndianSwapU32(image->header.image_size_bytes);
	image->header.x_resolution_ppm = EndianSwapU32(image->header.x_resolution_ppm);
	image->header.y_resolution_ppm = EndianSwapU32(image->header.y_resolution_ppm);
	image->header.num_colors = EndianSwapU32(image->header.num_colors);
	image->header.important_colors = EndianSwapU32(image->header.important_colors);
	
	return E_NONE;
}

uint8_t *BMP_GetPixelData(BMPImage *image)
{
	// Returns a pointer to the start of the BMP's pixel data.
	
	return ((uint8_t *)image)+image->header.offset;
}