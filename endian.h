#pragma once

#include <stdint.h>

static int16_t EndianSwapS16(int16_t num)
{
	return 	(num << 8) | ((num >> 8) & 0xFF);
}

static uint16_t EndianSwapU16(uint16_t num)
{
	return 	(num>>8) | (num<<8);
}

static uint32_t EndianSwapU32(uint32_t num)
{
	return 	((num>>24)&0xff) | 		// move byte 3 to byte 0
			((num<<8)&0xff0000) | 	// move byte 1 to byte 2
            ((num>>8)&0xff00) | 	// move byte 2 to byte 1
            ((num<<24)&0xff000000); // byte 0 to byte 3	
}

static int32_t EndianSwapS32( int32_t val )
{
    val = ((val << 8) & 0xFF00FF00) | ((val >> 8) & 0xFF00FF ); 
    return (val << 16) | ((val >> 16) & 0xFFFF);
}