#include "x68kgfx.h"

VIDCON_State vidcon_state;
VIDCON_Registers *vidcon_registers = (VIDCON_Registers *)0xE80000;

void VIDCON_GoMode8()
{
	vidcon_state.mode = 8;
	vidcon_state.px_rows = 512;
	vidcon_state.px_columns = 512;
	
	_iocs_crtmod(8);
	_iocs_vpage(0);
	_iocs_b_curoff();
	_iocs_g_clr_on();
};

void VIDCON_GoMode10()
{
	vidcon_state.mode = 10;
	vidcon_state.px_rows = 512;
	vidcon_state.px_columns = 512;
	
	_iocs_crtmod(10);
	_iocs_vpage(0);
	_iocs_b_curoff();
	_iocs_g_clr_on();
};


void VIDCON_Cleanup()
{
	_iocs_crtmod(0);
}