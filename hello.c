#include <stdio.h>
#include <stdlib.h>
#include <dos.h>
#include <iocs.h>
#include <assert.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>

#include "endian.h"
#include "bmp.h"
#include "x68kgfx.h"

#define BETWEEN(token, A, B) (token >= A && token <= B)
#define MMIO16(addr) (*(volatile uint16_t *)(addr))

typedef struct {
	int16_t x, y;
} Point;

void draw_line(Point start, Point end, uint8_t color)
{
    // Draw a 1-pixel thick line between two points.
    int x1 = start.x;
    int x2 = end.x;
    int y1 = start.y;
    int y2 = end.y;
	int temp;

    // Bresenham's algorithm. TODO: Fixed-point or integer implementation
    const bool steep = (fabs(y2 - y1) > fabs(x2 - x1));
    if(steep)
    {
		temp = y1;
		y1 = x1;
		x1 = temp;
        //swap(x1, y1);
		
		temp = y2;
		y2 = x2;
		x2 = temp;
        //swap(x2, y2);
    }

    if(x1 > x2){
		temp = x2;
		x2 = x1;
		x1 = temp;
        //swap(x1, x2);
		
		temp = y2;
		y2 = y1;
		y1 = temp;
        //swap(y1, y2);
    }

    const float dx = x2 - x1;
    const float dy = fabs(y2 - y1);

    float error = dx / 2.0f;
    const int ystep = (y1 < y2) ? 1 : -1;
    int y = (int)y1;

    const int maxX = (int)x2;

    for(int x=(int)x1; x<maxX; x++)
    {
        if(steep)
        {
            if(BETWEEN(x, 0, 512) && BETWEEN(y, 0, 512)){
                //setPixel(pixels, y, x, color);
		gvram[(512*x)+y] = color;
            }
        }
        else
        {
            if(BETWEEN(x, 0, 512) && BETWEEN(y, 0, 512)){
                //setPixel(pixels, x, y, color);
				gvram[(512*y)+x] = color;
            }
        }
 
        error -= dy;
        if(error < 0)
        {
            y += ystep;
            error += dx;
        }

    }
}

void WaitForVSYNC()
{
	while((MMIO16(0xE88000) & 0x10) > 0);
	while((MMIO16(0xE88000) & 0x10) == 0);
}

void BMP_GetPalette(uint16_t *buf, int start, int end, BMPImage *image)
{
	// buf: destination buffer
	// start: palette index of BMP to start at
	// end: palette index of BMP to end at
	// image: the BMPImage to use the palette of
	
	if(image->header.num_colors != 256)
	{
		printf("This is not a 256-color BMP, aborting palette load\r\n");
		return;
	}
	
	uint8_t *palette_data = (uint8_t *)&image->data;
	int dest_color = 0;
	
	for(int i=start; i<end; i++)
	{
		uint8_t r, g, b;
		b = *palette_data++;
		g = *palette_data++;
		r = *palette_data++;
		*palette_data++; // discard
		buf[dest_color++] = GRB555I(r, g, b, 1);
	}
}

int main(int argc, char *argv[])
{
	_dos_super(0);

	VIDCON_GoMode8();
	
	BMPImage *puni_data;
	BMP_Load((void **)&puni_data, "puni.bmp");
	
	uint16_t palette_buffer[256];
	BMP_GetPalette(palette_buffer, 0, 256, puni_data);
	memcpy(gfx_palette, palette_buffer, 512);
	
	uint8_t *puni_pixels = BMP_GetPixelData(puni_data);
	printf("pixels are at %p\r\n", puni_pixels);
	
	for(int i=0; i<puni_data->header.width_px*puni_data->header.height_px; i++)
	{
		gvram[i] = puni_pixels[i];
	}
	
	printf("Press any key to exit...\r\n");	
	
	bool done = false;
	int scrollx_add = 1;
	int scrolly_add = 1;
	
	int scrollx = 0;
	int scrolly = 0;
	
	while(!done)
	{
		scrollx += scrollx_add;
		scrolly += scrolly_add;
		
		if(scrollx > 85) scrollx_add = -1;
		if(scrollx < -85) scrollx_add = 1;
		
		if(scrolly > 140) scrolly_add = -1;
		if(scrolly < -140) scrolly_add = 1;
		
		vidcon_registers->bg0_scroll_x = scrollx;
		vidcon_registers->bg1_scroll_x = scrollx;
		
		vidcon_registers->bg0_scroll_y = scrolly;
		vidcon_registers->bg1_scroll_y = scrolly;
		
		if(_dos_keysns() != 0)
		{
			done = true;
		}
		
		WaitForVSYNC();
	}

	getchar();
	VIDCON_Cleanup();
}