# Names of the compiler and friends
APP_BASE = /opt/toolchains/x68k/bin/human68k
AS 		= $(APP_BASE)-as
CC 		= $(APP_BASE)-gcc
LD 		= $(APP_BASE)-ld
OBJCOPY 	= $(APP_BASE)-objcopy
STRIP 		= $(APP_BASE)-strip

# libraries and paths
LIBS	 	= -ldos -liocs
INCLUDES 	=          

# Compiler flags
ASM_FLAGS 	= -m68000 --register-prefx-optional
LDFLAGS 	=
CFLAGS 		= -m68000 -std=c99 -fomit-frame-pointer -Wall -Wno-unused-function -Wno-unused-variable -O3
LDSCRIPT 	=
OCFLAGS		= -O xfile

# What our application is named
TARGET	= hello
EXE	= $(TARGET).X

all: $(EXE)

OBJFILES = hello.o bmp.o x68kgfx.o

$(EXE): $(OBJFILES)
	$(CC) $(LDFLAGS) $(OBJFILES) $(LIBS) -o $(TARGET)
	$(OBJCOPY) $(OCFLAGS) $(TARGET) $(EXE)

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -f *.o $(EXE) $(TARGET)
