#pragma once

#include <stdint.h>

#include <dos.h>
#include <iocs.h>

// Convert RGB888 to GRB555I.
#define GRB555I(r, g, b, i) ( ((b&0xF8)>>2) | ((g&0xF8)<<8) | ((r&0xF8)<<3) | i )

#define GVRAM_START		0xC00000		// Start of GVRAM
#define GPAL_START		0xE82000		// Graphics palette
#define TPAL_START		0xE82200		// Text palette

static uint16_t *gvram = (uint16_t *)GVRAM_START;
static uint16_t *gfx_palette = (uint16_t *)GPAL_START;
static uint16_t *text_palette = (uint16_t *)TPAL_START;

#pragma pack(1)
typedef struct {
	uint16_t horizontal_total; // R00
	uint16_t hsync_end; // R01
	uint16_t horizontal_start; // R02
	uint16_t horizontal_end; // R03
	uint16_t vertical_total; // R04
	uint16_t vsync_end; // R05
	uint16_t vertical_start; // R06
	uint16_t vertical_end; // R07
	uint16_t horizontal_tuning; // R08
	uint16_t raster_number; // R09
	uint16_t text_x; // R10
	uint16_t text_y; // R11
	uint16_t bg0_scroll_x; // R12
	uint16_t bg0_scroll_y; // R13
	uint16_t bg1_scroll_x; // R14
	uint16_t bg1_scroll_y; // R15
	uint16_t bg2_scroll_x; // R16
	uint16_t bg2_scroll_y; // R17
	uint16_t bg3_scroll_x; // R18
	uint16_t bg3_scroll_y; // R19
	uint16_t r20; // R20
	uint16_t r21; // R21
	uint16_t r22; // R22
	uint16_t text_mask_pattern; // R23
} VIDCON_Registers;
#pragma pack()

extern VIDCON_Registers *vidcon_registers;

// Shadow VIDCON registers and graphics mode info
typedef struct {
	uint8_t mode;
	uint16_t px_rows;
	uint16_t px_columns;
} VIDCON_State;

extern VIDCON_State vidcon_state;

void VIDCON_GoMode8();
void VIDCON_GoMode10();
void VIDCON_Cleanup();